insert into users(id, username, password, email, active, deleted)
values(1, 'Xurshida', '$2a$10$0qyIHlDD2U4L36Ce1riTc.RbrPRM5icsEbK4CoCiuKkN18q4vDUJ2', 'shushi31072001@gmail.com', true, false);

insert into role(id, name) values
(1, 'PLAYER'),(2, 'USER'),(3, 'ADMIN');
