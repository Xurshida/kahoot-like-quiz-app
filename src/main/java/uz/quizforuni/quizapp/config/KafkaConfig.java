//package uz.quizforuni.quizapp.config;
//
//import org.apache.kafka.clients.admin.NewTopic;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.config.TopicBuilder;
//import uz.quizforuni.quizapp.constants.KafkaTopicKeysConstants;
//
//@Configuration
//public class KafkaConfig {
//
//    @Bean
//    public NewTopic quizPlaygroundTopic(){
//        return TopicBuilder.name(KafkaTopicKeysConstants.QUIZ_PLAYGROUND).build();
//    }
//}
