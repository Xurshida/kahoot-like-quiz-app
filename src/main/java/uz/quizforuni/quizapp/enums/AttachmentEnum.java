package uz.quizforuni.quizapp.enums;

public enum AttachmentEnum {
    USER_AVATAR,
    FILE,
    QUESTION_PHOTO;
}
