package uz.quizforuni.quizapp.enums;

public enum RoleEnum {
    PLAYER, USER, ADMIN
}
