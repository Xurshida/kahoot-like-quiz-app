package uz.quizforuni.quizapp.enums;

public enum LanguageEnum {

    UZ,
    RU,
    ENG;

    public LanguageEnum getDefaultLanguage(){
        return LanguageEnum.ENG;
    }

}
