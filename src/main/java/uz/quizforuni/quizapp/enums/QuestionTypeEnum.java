package uz.quizforuni.quizapp.enums;

public enum QuestionTypeEnum {
    TRUE_FALSE{
        @Override
        public int getValue() {
            return 2;
        }
    },
    SIMPLE{
        @Override
        public int getValue() {
            return 4;
        }
    };


    public abstract int getValue();
}
