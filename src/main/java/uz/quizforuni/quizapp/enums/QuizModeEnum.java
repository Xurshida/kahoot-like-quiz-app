package uz.quizforuni.quizapp.enums;

public enum QuizModeEnum {
    TEAM_MODE,
    CLASS_MODE
}
