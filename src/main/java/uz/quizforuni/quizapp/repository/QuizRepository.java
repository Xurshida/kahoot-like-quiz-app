package uz.quizforuni.quizapp.repository;

import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.quizforuni.quizapp.entity.Quiz;
import uz.quizforuni.quizapp.entity.User;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quiz, Long> {

    List<Quiz> findAllByAuthorIdAndDeletedIsFalse(Long author_id);

    @Query(value = "update Quiz set deleted = true where id in :ids")
    void deleteAllByIds(@Param("ids") List<Long> ids);
}
