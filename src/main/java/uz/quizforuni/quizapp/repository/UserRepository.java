package uz.quizforuni.quizapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.quizforuni.quizapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByDeletedIsFalse();

    Optional<User> getUserByEmailAndPasswordAndDeletedIsFalseAndActiveIsTrue(String username, String password);

    Optional<User> findByIdAndDeletedIsFalse(Long id);

    User findByActivationCode(String code);

    User findByEmailAndDeletedIsFalse(String email);
}
