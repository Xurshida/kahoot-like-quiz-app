package uz.quizforuni.quizapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.quizforuni.quizapp.entity.QuestionType;

public interface QuestionTypeRepository extends JpaRepository<QuestionType, Long> {
}
