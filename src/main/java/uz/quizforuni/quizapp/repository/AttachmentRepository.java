package uz.quizforuni.quizapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.quizforuni.quizapp.entity.Question;

public interface AttachmentRepository extends JpaRepository<Question, Long> {
}
