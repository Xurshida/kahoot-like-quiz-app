package uz.quizforuni.quizapp.repository;

import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.quizforuni.quizapp.entity.Question;
import uz.quizforuni.quizapp.entity.Quiz;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByQuizIdAndDeletedIsFalse(Long quizId);

    @Query(value = "update Question set deleted = true where id in :ids")
    void deleteAllByIds(@Param("ids") List<Long> ids);
}
