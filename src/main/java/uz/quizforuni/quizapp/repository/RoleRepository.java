package uz.quizforuni.quizapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.quizforuni.quizapp.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
}
