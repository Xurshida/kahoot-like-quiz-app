package uz.quizforuni.quizapp.security;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityAuditorAware implements AuditorAware<Long> {

    @Override
    public Optional<Long> getCurrentAuditor() {
        return Optional.ofNullable(getUserId());
    }

    public Long getUserId(){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        MyAuthUser myAuthUser = (MyAuthUser) securityContext.getAuthentication().getPrincipal();
        return myAuthUser.getId();
    }

}
