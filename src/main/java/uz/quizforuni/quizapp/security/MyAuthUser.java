package uz.quizforuni.quizapp.security;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MyAuthUser extends User {
    Long id;
    String username;
    String language;
    Set<String> roles;
    Boolean isAccountNonLocked;

    public MyAuthUser(String email,
                      String password,
                      Collection<? extends GrantedAuthority> authorities,
                      Long id,
                      String username,
                      String language) {
        super(email, password, authorities);
        this.id = id;
        this.username = username;
        this.language = language;
        this.roles = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.isAccountNonLocked();
    }
}
