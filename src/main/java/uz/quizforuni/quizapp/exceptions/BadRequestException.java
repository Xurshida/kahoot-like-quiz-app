package uz.quizforuni.quizapp.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

@Getter
@Setter
public class BadRequestException extends AbstractThrowableProblem {

    String userMessage;

    String developerMessage;

    Object data;

    public BadRequestException(String userMessage, String developerMessage, Object data) {
        super(null, userMessage, Status.BAD_REQUEST);
        this.userMessage = userMessage;
        this.developerMessage = developerMessage;
        this.data = data;
    }

    public BadRequestException(String userMessage) {
        super(null, userMessage, Status.BAD_REQUEST);
        this.userMessage = userMessage;
    }

}
