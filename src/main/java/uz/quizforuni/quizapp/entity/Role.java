package uz.quizforuni.quizapp.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
@Setter
@Getter
public class Role implements GrantedAuthority{
    @Id
    private Long id;

    private String name;

    @Override
    public String getAuthority() {
        return name;
    }
}
