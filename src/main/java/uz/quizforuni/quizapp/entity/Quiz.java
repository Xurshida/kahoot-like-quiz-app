package uz.quizforuni.quizapp.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import uz.quizforuni.quizapp.entity.baseEntities.BaseAuditEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Quiz extends BaseAuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author")
    private User author;

    @OneToMany(mappedBy = "quiz", fetch = FetchType.LAZY)
    private Set<Question> questions;

    public Quiz() {
    }

    public Quiz(String name, String text, User author) {
        this.name = name;
        this.text = text;
        this.author = author;
    }
}

