package uz.quizforuni.quizapp.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.quizforuni.quizapp.enums.AttachmentEnum;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "attachment")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Attachment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String contentType;

    Long size;

    byte[] bytes;

    String name;

    @Enumerated(EnumType.STRING)
    AttachmentEnum attachmentEnum;

    boolean deleted;

}
