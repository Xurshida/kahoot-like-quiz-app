package uz.quizforuni.quizapp.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import uz.quizforuni.quizapp.entity.baseEntities.BaseEntity;
import uz.quizforuni.quizapp.enums.LanguageEnum;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(
        name = "users",
        indexes = @Index(columnList = "email"),
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"username","email"}
        )
)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User extends BaseEntity {

    String username;

    String password;

    boolean active;

    String email;

    String activationCode;

    boolean deleted = false;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(insertable = false,updatable = false,unique = true)
    Attachment avatar;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = { @JoinColumn(name = "user_id")},
            inverseJoinColumns = { @JoinColumn(name = "role_id")}
    )
    Set<Role> roles;

    @Enumerated(EnumType.STRING)
    LanguageEnum language = LanguageEnum.ENG;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<Quiz> quizzes;

}
