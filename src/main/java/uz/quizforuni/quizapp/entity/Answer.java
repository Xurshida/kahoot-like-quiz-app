package uz.quizforuni.quizapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.quizforuni.quizapp.entity.baseEntities.BaseAuditEntity;
import uz.quizforuni.quizapp.entity.baseEntities.BaseEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Answer extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Question question;

    @OneToOne(fetch = FetchType.LAZY)
    private Variant chosenVariant;

    private Integer step;

    private boolean correct;

}
