package uz.quizforuni.quizapp.entity;

import lombok.Getter;
import lombok.Setter;
import uz.quizforuni.quizapp.entity.baseEntities.BaseEntity;
import uz.quizforuni.quizapp.enums.QuestionTypeEnum;

import javax.persistence.Entity;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
public class QuestionType extends BaseEntity {
    @Enumerated
    private QuestionTypeEnum questionTypeEnum;
}
