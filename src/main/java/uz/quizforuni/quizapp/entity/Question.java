package uz.quizforuni.quizapp.entity;

import lombok.Getter;
import lombok.Setter;
import uz.quizforuni.quizapp.entity.baseEntities.BaseAuditEntity;
import uz.quizforuni.quizapp.enums.QuestionTypeEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Question extends BaseAuditEntity {

    private String text;

    private Integer seconds = 30;

    @Enumerated(EnumType.STRING)
    private QuestionTypeEnum questionType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Variant> options;
}
