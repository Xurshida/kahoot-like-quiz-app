package uz.quizforuni.quizapp.constants;

public interface MessageConstants {

//    entity
    String entityNotFound = "entity.not.found";
    String entityNotFoundByParam = "entity.not.found.by.param";

//    entity fields
    String lowerId = "id";
    String biggerId = "Id";
    String bigId = "ID";

//    responses messages
    String messageSuccess = "api.response.success";
    String messageUserExists = "api.response.user.exists";
    String messageError = "server.error";
    String messageInvalidParams = "api.response.invalid.params";
    String responseMessage="";

    //    responses codes
    Integer codeSuccess = 200;
    Integer messageNotFound = 211;
    Integer codeError = 500;
    Integer codeClientError = 400;

}
