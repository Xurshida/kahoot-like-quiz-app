package uz.quizforuni.quizapp.constants;

public interface KafkaTopicKeysConstants {
    String QUIZ_PLAYGROUND = "quiz_playground";
    String ERROR_LOGS = "error_logs";
    String PASSWORD_RECOVERY_NOTIFICATION = "password_recovery_notification";

}
