package uz.quizforuni.quizapp.constants;

public interface RedisKeyConstants {
    String userList = "user.list";
    String quizList = "quiz.list";
    String questionTypeList = "question.type.list";
    String questionList = "question.list";
}
