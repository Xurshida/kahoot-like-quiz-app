package uz.quizforuni.quizapp.constants;

public interface ApiConstants {
    String api = "/api";
    String login = "/login";
    String register = "/register";
    String activate = "/activate/{code}";
    String upload = "/upload";
    String download = "/download";
    String verifyCode = "/verifyCode";

    String auth = "/auth";
    String quiz = "/quiz";
    String profileEdit = "/profile/edit";
    String answer = "/answer";
    String question = "/question";
    String user = "/user";
    String file = "/file";

    String authRootApi = api + auth;
    String userRootApi = api + user;
    String questionRootApi = api + question;
    String quizRootApi = api + quiz;
    String attachmentRootApi = api + file;

    String create = "/create";
    String update = "/update";
    String delete = "/delete";
    String get = "/get";

    String byId = "/{id}";
}
