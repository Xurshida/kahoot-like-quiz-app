package uz.quizforuni.quizapp.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.entity.User;
import uz.quizforuni.quizapp.enums.RoleEnum;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.payload.ReqProfile;
import uz.quizforuni.quizapp.dto.payload.SignIn;
import uz.quizforuni.quizapp.dto.payload.SignUp;
import uz.quizforuni.quizapp.repository.RoleRepository;
import uz.quizforuni.quizapp.repository.UserRepository;
import uz.quizforuni.quizapp.security.MyAuthUser;
import uz.quizforuni.quizapp.security.MyPasswordEncoder;
import uz.quizforuni.quizapp.security.jwt.JwtTokenProvider;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AuthService extends BaseService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final MailSender mailSender;
    private final MyPasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final AuthenticationManager authenticationManager;

    public AuthService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       MailSender mailSender,
                       @Lazy MyPasswordEncoder passwordEncoder,
                       JwtTokenProvider jwtTokenProvider,
                       @Lazy AuthenticationManagerBuilder authenticationManagerBuilder,
                       @Lazy AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.authenticationManager = authenticationManager;
    }

    @Value("${server.address}")
    String serverAddress;

    @Value("${server.port}")
    String serverPort;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmailAndDeletedIsFalse(username);

        if (user == null) {
            throw new UsernameNotFoundException("No such user: " + username);
        }

        return getMyAuthUser(user);
    }

    public MyAuthUser loadUserById(Long id) throws UsernameNotFoundException {
        User user = userRepository.findByIdAndDeletedIsFalse(id).orElseThrow();

        if (user == null) {
            throw new UsernameNotFoundException("No such user: " + id);
        }

        return getMyAuthUser(user);
    }

    private MyAuthUser getMyAuthUser(User user){
        return new MyAuthUser(
                user.getEmail(),
                user.getPassword(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList()),
                user.getId(),
                user.getUsername(),
                user.getLanguage().name());
    }

    public ApiResponse<?> login(SignIn signIn) {
        ApiResponse<Map<String, String>> response = new ApiResponse<>();
        try {
            if (userRepository.getUserByEmailAndPasswordAndDeletedIsFalseAndActiveIsTrue(
                    signIn.getEmail(), passwordEncoder.encode(signIn.getPassword()))
                    .isPresent()) {
                response.setMessage("Success");
                response.setCode(MessageConstants.codeSuccess);
                response.setData(Map.of("token", getApiToken(signIn.getEmail(), signIn.getPassword())));
            } else {
                response.setMessage("Login or password is incorrect");
                response.setCode(MessageConstants.codeError);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    private String getApiToken(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtTokenProvider.generateToken(authentication);
    }

    public ApiResponse<?> register(SignUp signUp) {
        ApiResponse<?> response = new ApiResponse<>();
        try{
            User userFromDB = userRepository.findByEmailAndDeletedIsFalse(signUp.getEmail());

            if (userFromDB != null) {
                response.setCode(MessageConstants.codeClientError);
                response.setMessage(MessageConstants.messageUserExists);
            }
            User newUser = new User();
            newUser.setActive(false);
            newUser.setRoles(Collections.singleton(roleRepository.findByName(RoleEnum.USER.name()).orElseThrow()));
            newUser.setActivationCode(UUID.randomUUID().toString());
            newUser.setPassword(passwordEncoder.encode(signUp.getPassword()));

            userRepository.save(newUser);

            sendActivationCode(newUser);
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }

    public ApiResponse<?> updateProfile(ReqProfile reqProfile) {
        ApiResponse<?> response = new ApiResponse<>();
        try {
            User user = userRepository.findByIdAndDeletedIsFalse(getCurrentUser().getId()).orElseThrow();
            if (!StringUtils.hasText(reqProfile.getPassword())) {
                user.setPassword(passwordEncoder.encode(reqProfile.getPassword()));
            }
            if (!StringUtils.hasText(reqProfile.getUsername())) {
                user.setPassword(passwordEncoder.encode(reqProfile.getUsername()));
            }
            userRepository.save(user);

            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
        }catch (Exception e){
            System.out.println(e.getMessage());
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }

    private void sendActivationCode(User user) {
        try {
            if (!StringUtils.isEmpty(user.getEmail())) {
                String text = String.format("Hello, %s! \n" +
                            "Welcome to Quiz! \n" +
                            "Please, go to this link to activate your account:" +
                            serverAddress + ":" + serverPort + "/activate/%s",
                    user.getUsername(),
                    user.getActivationCode());
                mailSender.send(user.getEmail(), "Activation code", text);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);

        if (user == null) {
            return false;
        }

        user.setActive(true);
        user.setActivationCode(null);
        userRepository.save(user);

        return true;
    }
}
