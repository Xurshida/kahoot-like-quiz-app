package uz.quizforuni.quizapp.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.constants.RedisKeyConstants;
import uz.quizforuni.quizapp.entity.User;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.repository.UserRepository;

import java.util.List;
import java.util.Map;

@Service
public class UserService extends BaseService{
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Cacheable(cacheNames = RedisKeyConstants.userList)
    @Transactional(readOnly = true)
    public ApiResponse<?> getAllUsers(){
        ApiResponse<Map<String, Object>> response = new ApiResponse<>();
        try{
            List<User> users = userRepository.findAllByDeletedIsFalse();
            response.setMessage(MessageConstants.messageSuccess);
            response.setCode(MessageConstants.codeSuccess);
            response.setData(Map.of("users", users));
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }
}
