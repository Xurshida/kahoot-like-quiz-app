package uz.quizforuni.quizapp.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.constants.RedisKeyConstants;
import uz.quizforuni.quizapp.dto.IdList;
import uz.quizforuni.quizapp.entity.Quiz;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.payload.ReqQuiz;
import uz.quizforuni.quizapp.repository.QuizRepository;
import uz.quizforuni.quizapp.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuizService extends BaseService{
    private QuizRepository quizRepository;
    private UserRepository userRepository;

    public QuizService(QuizRepository quizRepository, UserRepository userRepository) {
        this.quizRepository = quizRepository;
        this.userRepository = userRepository;
    }

    @Cacheable(cacheNames = RedisKeyConstants.quizList, key = "#userId")
    @Transactional(readOnly = true)
    public ApiResponse<?> getQuizzesByUser(Long userId){
        ApiResponse<List<uz.quizforuni.quizapp.dto.Quiz>> response = new ApiResponse<>();
        try {
            List<Quiz> quizzes = quizRepository.findAllByAuthorIdAndDeletedIsFalse(userId);
            response.setData(quizzes.stream().map(quiz -> new uz.quizforuni.quizapp.dto.Quiz(
                    quiz.getId(),
                    quiz.getName(),
                    quiz.getText(),
                    quiz.getAuthor())).collect(Collectors.toList()));
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }

    @CacheEvict(cacheNames = RedisKeyConstants.quizList, allEntries = true)
    public ApiResponse<?> save(ReqQuiz reqQuiz){
        ApiResponse<?> response = new ApiResponse<>();
        try {
            Quiz quiz = new Quiz();
            quiz.setId(quiz.getId());
            quiz.setAuthor(userRepository.findById(getCurrentUser().getId()).orElseThrow());
            quiz.setName(reqQuiz.getName());
            quizRepository.save(quiz);
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }

    @CacheEvict(cacheNames = RedisKeyConstants.quizList, allEntries = true)
    public ApiResponse<?> delete(IdList idList){
        ApiResponse<?> response = new ApiResponse<>();
        try {
            quizRepository.deleteAllByIds(idList.getIds());
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(MessageConstants.messageError);
        }
        return response;
    }
}
