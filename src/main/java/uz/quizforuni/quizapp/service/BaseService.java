package uz.quizforuni.quizapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.exceptions.NotFoundException;
import uz.quizforuni.quizapp.security.MyAuthUser;

import java.io.Serializable;
import java.util.function.Supplier;

public class BaseService implements Serializable {

    LocalizationService localizationService;

    @Autowired
    public void setLocalizationService(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    protected Supplier<NotFoundException> notFoundExceptionThrow(String message){
        return ()-> new NotFoundException(message);
    }

    protected String localizeMessage(String code){
        return localizationService.localize(code);
    }

    protected static MyAuthUser getCurrentUser() {
        MyAuthUser user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof MyAuthUser) {
                user = (MyAuthUser) authentication.getPrincipal();
            }
        }
        return user;
    }

    protected ApiResponse<?> entityNotFoundMapper(String entityName, String field, String value){
        ApiResponse<?> apiResponse = new ApiResponse<>();
        apiResponse.setCode(MessageConstants.messageNotFound);
        apiResponse.setMessage(
                localizationService.localize(
                        MessageConstants.entityNotFoundByParam,
                        new Object[]{entityName, field, value},
                        localizationService.getLocale()
                )
        );
        return apiResponse;
    }

}
