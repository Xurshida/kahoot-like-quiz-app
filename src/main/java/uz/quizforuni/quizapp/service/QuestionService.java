package uz.quizforuni.quizapp.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.constants.RedisKeyConstants;
import uz.quizforuni.quizapp.dto.IdList;
import uz.quizforuni.quizapp.entity.Question;
import uz.quizforuni.quizapp.entity.QuestionType;
import uz.quizforuni.quizapp.entity.Variant;
import uz.quizforuni.quizapp.enums.QuestionTypeEnum;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.payload.ReqQuestion;
import uz.quizforuni.quizapp.repository.QuestionRepository;
import uz.quizforuni.quizapp.repository.QuestionTypeRepository;
import uz.quizforuni.quizapp.repository.QuizRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionService extends BaseService {

    private QuestionRepository questionRepository;
    private QuizRepository quizRepository;
    private QuestionTypeRepository questionTypeRepository;

    private QuestionService(QuestionRepository questionRepository, QuizRepository quizRepository, QuestionTypeRepository questionTypeRepository) {
        this.questionRepository = questionRepository;
        this.quizRepository = quizRepository;
        this.questionTypeRepository = questionTypeRepository;
    }

    @Cacheable(cacheNames = RedisKeyConstants.questionList, key = "#quizId")
    public ApiResponse<?> list(Long quizId){
        ApiResponse<List<uz.quizforuni.quizapp.dto.Question>> response = new ApiResponse<>();
        try {
            List<Question> questions = questionRepository.findAllByQuizIdAndDeletedIsFalse(quizId);
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
            response.setData(questions.stream().map(question -> new uz.quizforuni.quizapp.dto.Question(
                    question.getId(),
                    question.getText(),
                    question.getQuestionType().name(),
                    question.getOptions())).collect(Collectors.toList()));
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(localizeMessage(MessageConstants.messageError));
        }
        return response;
    }

    public ApiResponse<?> add(ReqQuestion reqQuestion) {
        ApiResponse<List<Question>> response = new ApiResponse<>();
        try {
            if (reqQuestion.getType().equals(QuestionTypeEnum.TRUE_FALSE.name()) && reqQuestion.getVariants().size() != QuestionTypeEnum.TRUE_FALSE.getValue() ||
                    reqQuestion.getType().equals(QuestionTypeEnum.SIMPLE.name()) && reqQuestion.getVariants().size() != QuestionTypeEnum.SIMPLE.getValue()){
                response.setCode(MessageConstants.codeClientError);
                response.setMessage(localizeMessage(MessageConstants.messageInvalidParams));
                return response;
            }
            Question question = new Question();
            question.setQuestionType(QuestionTypeEnum.valueOf(reqQuestion.getType()));
            question.setQuiz(quizRepository.getById(reqQuestion.getQuizId()));
            question.setOptions(reqQuestion.getVariants().stream().map(reqVariant ->
                    new Variant(reqVariant.getId(), reqVariant.getText(), reqVariant.isCorrect())).collect(Collectors.toSet()));
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(localizeMessage(MessageConstants.messageSuccess));
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(localizeMessage(MessageConstants.messageError));
        }
        return response;
    }

    @Cacheable(cacheNames = RedisKeyConstants.questionTypeList)
    public ApiResponse<?> getQuestionTypes() {
        ApiResponse<List<QuestionType>> response = new ApiResponse<>();
        try {
            response.setData(questionTypeRepository.findAll());
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(localizeMessage(MessageConstants.messageSuccess));
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(localizeMessage(MessageConstants.messageError));
        }
        return response;
    }

    public ApiResponse<?> delete(IdList idList){
        ApiResponse<?> response = new ApiResponse<>();
        try {
            questionRepository.deleteAllByIds(idList.getIds());
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(localizeMessage(MessageConstants.messageSuccess));
        }catch (Exception e){
            response.setCode(MessageConstants.codeError);
            response.setMessage(localizeMessage(MessageConstants.messageError));
        }
        return response;
    }
}
