package uz.quizforuni.quizapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import uz.quizforuni.quizapp.enums.LanguageEnum;

import java.util.Locale;

@Service
public class LocalizationService {

    @Autowired
    MessageSource messageSource;

    protected static LanguageEnum getCurrentUserLanguageEnum() {
        if (BaseService.getCurrentUser() != null) {
            return LanguageEnum.valueOf(BaseService.getCurrentUser().getLanguage());
        }
        return LanguageEnum.ENG;
    }

    protected Locale getLocale(){
        return Locale.forLanguageTag(getCurrentUserLanguageEnum().name());
    }

    public String localize(String code){
        return localize(code, null, getLocale());
    }

    public String localize(String code, Object[] params, Locale locale){
        if (code.isEmpty()){
            return null;
        }
        return messageSource.getMessage(code, params, locale);
    }

}
