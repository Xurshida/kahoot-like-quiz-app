package uz.quizforuni.quizapp.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.security.MyAuthUser;

import java.util.Map;
import java.util.Random;

@RestController
public class PlayGroundHostController {
    private KafkaTemplate<String, Object> kafkaTemplate;
    private RedisTemplate<String, String> redisTemplate;

    public PlayGroundHostController(KafkaTemplate<String, Object> kafkaTemplate, RedisTemplate<String, String> redisTemplate) {
        this.kafkaTemplate = kafkaTemplate;
        this.redisTemplate = redisTemplate;
    }

    @PostMapping("/play") // for host
    public HttpEntity<?> play(@AuthenticationPrincipal MyAuthUser myAuthUser, @RequestParam Long quizId) {
        ApiResponse<Map<String, String>> response = new ApiResponse<>();
        Random random = new Random();
        String number = String.format("%06d", random.nextInt(999999));
        response.setCode(MessageConstants.codeSuccess);
        response.setMessage(MessageConstants.messageSuccess);
        response.setData(Map.of("game_id", number));
        return ResponseEntity.status(response.getCode()).body(response);
    }

}
