package uz.quizforuni.quizapp.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.quizforuni.quizapp.constants.ApiConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.payload.ReqProfile;
import uz.quizforuni.quizapp.service.AuthService;
import uz.quizforuni.quizapp.service.UserService;

import static uz.quizforuni.quizapp.constants.ApiConstants.userRootApi;

@RestController
@RequestMapping(userRootApi)
public class UserController {

    private final AuthService authService;
    private final UserService userService;

    public UserController(AuthService authService, UserService userService) {
        this.authService = authService;
        this.userService = userService;
    }


    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public HttpEntity<?> usersList() {
        ApiResponse<?> response = userService.getAllUsers();
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping(ApiConstants.profileEdit)
    public HttpEntity<?> updateProfile(
            @RequestBody ReqProfile reqProfile
            ) {
        ApiResponse<?> response = authService.updateProfile(reqProfile);
        return ResponseEntity.status(response.getCode()).body(response);
    }
}
