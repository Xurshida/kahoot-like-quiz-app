package uz.quizforuni.quizapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import uz.quizforuni.quizapp.constants.ApiConstants;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.ValidationErrorMessage;
import uz.quizforuni.quizapp.dto.payload.SignIn;
import uz.quizforuni.quizapp.dto.payload.SignUp;
import uz.quizforuni.quizapp.service.AuthService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(ApiConstants.authRootApi)
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping(ApiConstants.login)
    public HttpEntity<?> login(@RequestBody SignIn signIn) {
        return ResponseEntity.ok(authService.login(signIn));
    }

    @PostMapping(ApiConstants.register)
    public HttpEntity<?> registration(@Valid @RequestBody SignUp signUp, BindingResult bindingResult) {
        ApiResponse<List<ValidationErrorMessage>> response = new ApiResponse<>();
        if (bindingResult.hasErrors()){
            List<ValidationErrorMessage> errorMessages = bindingResult.getAllErrors().stream().map(objectError ->
                    new ValidationErrorMessage(
                            ((FieldError)objectError).getField(), objectError.getDefaultMessage())).collect(Collectors.toList());
            response.setCode(MessageConstants.codeClientError);
            response.setMessage(MessageConstants.messageInvalidParams);
            response.setData(errorMessages);
            return ResponseEntity.status(response.getCode()).body(response);
        }
        return ResponseEntity.ok(authService.register(signUp));
    }

    @PostMapping(ApiConstants.activate)
    public HttpEntity<?> activate(@PathVariable String code) {
        return ResponseEntity.ok(authService.activateUser(code));
    }
}
