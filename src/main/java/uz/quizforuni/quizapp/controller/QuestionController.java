package uz.quizforuni.quizapp.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.quizforuni.quizapp.constants.ApiConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.IdList;
import uz.quizforuni.quizapp.dto.payload.ReqQuestion;
import uz.quizforuni.quizapp.service.QuestionService;

@RestController
@RequestMapping(ApiConstants.questionRootApi)
public class QuestionController {

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/type")
    @Operation(description = "getting question types")
    public HttpEntity<?> questionTypesList(){
        ApiResponse<?> response = questionService.getQuestionTypes();
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @GetMapping("/list")
    @Operation(description = "getting questions")
    public HttpEntity<?> list(@RequestParam("quiz_id") Long quizId){
        ApiResponse<?> response = questionService.list(quizId);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping("/save")
    public HttpEntity<?> add(@RequestBody ReqQuestion reqQuestion){
        ApiResponse<?> response = questionService.add(reqQuestion);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping("/delete")
    public HttpEntity<?> delete(@RequestBody IdList idList){
        ApiResponse<?> response = questionService.delete(idList);
        return ResponseEntity.status(response.getCode()).body(response);
    }

}
