package uz.quizforuni.quizapp.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.quizforuni.quizapp.constants.ApiConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.IdList;
import uz.quizforuni.quizapp.dto.payload.ReqQuiz;
import uz.quizforuni.quizapp.service.QuizService;

@RestController
@RequestMapping(ApiConstants.quizRootApi)
public class QuizController {
    private final QuizService quizService;

    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping("/list")
    @Operation(description = "getting quizzes by user")
    public HttpEntity<?> list(@RequestParam("user_id") Long userId){
        ApiResponse<?> response = quizService.getQuizzesByUser(userId);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody ReqQuiz reqQuiz){
        ApiResponse<?> response = quizService.save(reqQuiz);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping("/delete")
    public HttpEntity<?> delete(@RequestBody IdList idList){
        ApiResponse<?> response = quizService.delete(idList);
        return ResponseEntity.status(response.getCode()).body(response);
    }
}
