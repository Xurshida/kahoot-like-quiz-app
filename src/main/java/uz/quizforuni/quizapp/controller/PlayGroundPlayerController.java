package uz.quizforuni.quizapp.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.quizforuni.quizapp.constants.KafkaTopicKeysConstants;
import uz.quizforuni.quizapp.constants.MessageConstants;
import uz.quizforuni.quizapp.dto.ApiResponse;
import uz.quizforuni.quizapp.dto.kafka.SubscribedUser;

@RestController
public class PlayGroundPlayerController {
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final RedisTemplate<String, String> redisTemplate;

    public PlayGroundPlayerController(KafkaTemplate<String, Object> kafkaTemplate, RedisTemplate<String, String> redisTemplate) {
        this.kafkaTemplate = kafkaTemplate;
        this.redisTemplate = redisTemplate;
    }

    @PostMapping("/subscribe")
    public HttpEntity<?> subscribe(@RequestParam String gameId, @RequestParam String username) {
        ApiResponse<?> response = new ApiResponse<>();
        //find out if username already exists, if not add to topic
        if(Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(gameId, username))){
            redisTemplate.opsForSet().add(gameId, username);
            kafkaTemplate.send(KafkaTopicKeysConstants.QUIZ_PLAYGROUND, gameId, new SubscribedUser(gameId, username));
            response.setCode(MessageConstants.codeSuccess);
            response.setMessage(MessageConstants.messageSuccess);
        }else {
            response.setCode(MessageConstants.codeClientError);
            response.setMessage(MessageConstants.messageUserExists);
        }
        return ResponseEntity.status(response.getCode()).body(response);
    }

}
