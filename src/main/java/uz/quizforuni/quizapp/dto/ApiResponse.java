package uz.quizforuni.quizapp.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ApiResponse<T> {

    String message;

    Integer code;

    T data;

}
