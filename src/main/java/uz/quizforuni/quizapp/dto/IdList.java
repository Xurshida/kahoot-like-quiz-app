package uz.quizforuni.quizapp.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class IdList {
    List<Long> ids;
}
