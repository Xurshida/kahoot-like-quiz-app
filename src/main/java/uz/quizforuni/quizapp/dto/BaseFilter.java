package uz.quizforuni.quizapp.dto;


import io.netty.util.internal.StringUtil;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.sql.Timestamp;

@Setter
@Getter
@FieldDefaults(level = AccessLevel.PROTECTED)
public class BaseFilter {

    Integer page = 0;

    Integer size = 15;

    String search = "";

//    @ApiModelProperty("From")
    Timestamp from;

//    @ApiModelProperty("To")
    Timestamp to;

    @Operation(hidden = true)
    public boolean isNotEmpty(){
        return !(search == null || search.length() == 0);
    }

    @Operation(hidden = true)
    public Pageable getPageable() {
        return PageRequest.of(this.getPage(), this.getSize());
    }

    @Operation(hidden = true)
    public int getStart(){
        return this.getPage() * this.getSize();
    }

    @Operation(hidden = true)
    public String getNativeLikeSearchKey(){
        return "like %".concat(this.getSearch().toLowerCase()).concat("%");
    }

    @Operation(hidden = true)
    public String getSearchForQuery() {
        return StringUtil.isNullOrEmpty(search) ? ("%".concat(this.getSearch().toLowerCase().concat("%"))) : search;
    }

    @Operation(hidden = true)
    public String getLikeSearchKey() {
        return " like (:searchKey)";
    }




}
