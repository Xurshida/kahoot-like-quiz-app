package uz.quizforuni.quizapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.quizforuni.quizapp.entity.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Quiz {
    private Long id;

    private String name;

    private String text;

    private User author;
}
