package uz.quizforuni.quizapp.dto;

public class Score {
    private Long questionId;
    private Integer score;
    private Integer ranking;
}
