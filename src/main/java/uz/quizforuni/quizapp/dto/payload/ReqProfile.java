package uz.quizforuni.quizapp.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReqProfile {
    @NotBlank(message = "Username can't be empty!")
    private String username;

    @NotBlank(message = "Password can't be empty!")
    private String password;
}
