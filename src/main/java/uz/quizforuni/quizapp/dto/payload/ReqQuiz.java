package uz.quizforuni.quizapp.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ReqQuiz {
    private Long id;

    @NotBlank(message = "Name of quiz can't be empty!")
    private String name;

    @NotBlank(message = "Description can't be empty!")
    @Length(max = 2048, message = "Too long description!")
    private String text;

    private String tag;

}
