package uz.quizforuni.quizapp.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReqQuestion {
    private Long id;
    private String title;
    private String type;
    private Long quizId;
    private List<ReqVariant> variants;
    private MultipartFile questionPhoto;
}
