package uz.quizforuni.quizapp.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignUp {
    @NotBlank(message = "Email can't be empty!")
    @Email(message = "Email isn't correct")
    private String email;

    private String username;

    @NotBlank(message = "Password can't be empty!")
    private String password;
}
