package uz.quizforuni.quizapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Answer {
    private Long userId;
    private Long chosenVariant;
}
