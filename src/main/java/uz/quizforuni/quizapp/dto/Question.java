package uz.quizforuni.quizapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.quizforuni.quizapp.entity.Variant;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Question {

    private Long id;

    private String text;

    private String questionType;

    private Set<Variant> options;
}
